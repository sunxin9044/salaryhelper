//
//  BackgroundClock.m
//  SalaryHelper
//
//  Create a clock in background using NSTimer to count the time and update the calendar
//  when it reach a new day or month or year
//
//  Created by Turbo on 7/14/15.
//  Copyright (c) 2015 Xin. All rights reserved.
//

#import "BackgroundClock.h"
@interface BackgroundClock()
@property (strong,nonatomic) NSTimer *timer; // Create a background timer to count the time change when app is opening, TEST PURPOSE

@property (nonatomic) NSInteger sec;
@property (nonatomic) NSInteger min;
@property (nonatomic) NSInteger hour;

@end

@implementation BackgroundClock

/*
 * Clock Object
 */
- (BackgroundClock *)initClockInBackgroundWithCurrentTime:(NSInteger)hour andMin:(NSInteger)min andSec:(NSInteger)sec
{
    self = [super init];
    
    if (self) {
        self.min = min;
        self.hour = hour;
        self.sec = sec;
        // Create the background timer
        if (self.timer) {
            [self.timer invalidate];
        }
    }
    return self;
}

- (void)start {
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(handleTimer:) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes]; // run in this mode, the timer will not interpret by user interactions such as scroll and tap.
}

- (void)stop {
    if (self.timer) {
        [self.timer invalidate];
    }
}

- (void)handleTimer:(NSTimer *)theTimer
{
    if (self.sec < 59) {
        self.sec ++;
    } else {
        self.sec = 0; // reset zero
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadNewDateForCal" object:nil];
        
        if (self.min == 59) {
            self.min = 0;
            if (self.hour == 23) { // reach the new day
                self.hour = 0;
                
                //Send notification that new has come, update the cal
            } else {
                self.hour++;
            }
        } else {
            self.min ++;
        }
    }
    
    [self displayTime];
}

- (void)displayTime
{
    NSLog(@"%d:%d:%d", self.hour, self.min, self.sec);
}

@end
