//
//  BackgroundClock.h
//  SalaryHelper
//
//  Created by Xin on 7/14/15.
//  Copyright (c) 2015 Xin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface BackgroundClock : NSObject

- (BackgroundClock *)initClockInBackgroundWithCurrentTime:(NSInteger)hour andMin:(NSInteger)min andSec:(NSInteger)sec;

- (void)start;
- (void)stop;

@end